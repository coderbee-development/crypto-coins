import { observable, toJS } from 'mobx';
import alt from './alt';

import AppActions from './actions';
import AppDataSource from './source';

class AppStore {
  constructor() {
    this.state = observable({
      globalProgress: false,
      globalMsg: '',
      feed: {},
      currencySelection: 'USD',
    });

    this.bindActions(AppActions);
    this.registerAsync(AppDataSource);
  }

  onSetCurrency(currency) {
    this.state.currencySelection = currency;
    AppActions.getAppFeed.defer();
  }

  onActivateGlobalProgress() {
    this.state.globalProgress = true;
  }

  onDeactivateGlobalProgress() {
    this.state.globalProgress = false;
  }

  onSetGlobalMsg(message) {
    this.state.globalMsg = message;
  }

  onGetAppFeed() {
    AppActions.activateGlobalProgress.defer();
    this.getInstance().getAppFeed(this.state.currencySelection);
  }

  onGetAppFeedSuccess(feedData) {
    this.state.feed = feedData;
    AppActions.deactivateGlobalProgress.defer();
  }

  // eslint-disable-next-line class-methods-use-this
  onGetAppFeedError() {
    AppActions.deactivateGlobalProgress.defer();
    AppActions.setGlobalMsg.defer('Throttled: Max number of requests made, try again in 24 hours');
  }

  static shouldShowGlobalProgress() {
    return this.getState().globalProgress;
  }

  static getGlobalMsg() {
    return this.getState().globalMsg;
  }

  static getFeed() {
    return toJS(this.getState().feed);
  }

  static getSelectedCurrency() {
    return this.getState().currencySelection;
  }
}

export default alt.createStore(AppStore);
