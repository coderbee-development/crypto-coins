import request from 'superagent';
import { sourceMethod } from './lib/utils/async-actions';
import AppActions from './actions';

const host = 'https://api.coinmarketcap.com';

const AppDataSource = {
  ...sourceMethod('getAppFeed', AppActions, async (state, currency) => {
    try {
      const res = await request.get(`${host}/v1/ticker/?limit=10&convert=${currency}`);
      return res.body;
    } catch (err) {
      // We want to handle the error without throwing
      // eslint-disable-next-line prefer-promise-reject-errors
      return Promise.reject('Data feed is currently unavailable');
    }
  }),
};

export default AppDataSource;
